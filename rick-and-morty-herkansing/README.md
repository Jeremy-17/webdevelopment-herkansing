# Getting Started with The Rick & Morty App

With this link the app can be cloned: [Repository](https://gitlab.com/Jeremy-17/webdevelopment-herkansing.git)

In the project directory, you can run:

### `npm install`

installs the missing node_modules onto youre device.


### `npm start`

Before you can run this app you need to install the missing modules!

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

<br>

# Voor de Docent
vorige keer vond u dat ik te weinig had gedaan, ik hoop met deze veranderingen het vak wel te halen.

* De home page vernieuwd
* De paginatie vernieuwd 
* In de character info page worden ook de episodes  
  weergegeven waar de character in voorkomt
* Een zoek functie toegevoegd
* Gebruikt gemaakt van ReactStrap & Bootstrap voor de UI 
* Design beetje aangepast

Hopelijk is dit voldoende toevoeging om uw vak te mogen behalen