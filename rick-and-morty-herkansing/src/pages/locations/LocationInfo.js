import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import {
  Card,
  CardText,
  CardBody,
  CardTitle,
} from "reactstrap";
import "./Locations.css";

function LocationInfo() {
  const { id } = useParams();
  const url = "https://rickandmortyapi.com/api/location/" + id;
  const [LocationData, setLocationData] = useState([]);
  const [LocationResidents, setLocationResidents] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const request = await axios.get(url);
      setLocationData(request.data);

      let residentsUrls = [];
      let residentText = "";
      for (var i = 0; i < request.data.residents.length; i++) {
        residentsUrls.push(axios.get(request.data.residents[i]));
      }

      axios.all(residentsUrls).then((response) => {
        response.forEach(function (res) {
          residentText = residentText += `<a href="/characters/${res.data.id}"> 
                                            <div class="resident">
                                                <img src="${res.data.image}"/>
                                                ${res.data.name}
                                            </div>
                                          </a>`;
        });
        setLocationResidents(residentText);
      });

      return request;
    }
    fetchData();
  }, [url]);

  console.log(LocationData);

  return (
    <div className="container">
      <Card className="location">
        <CardBody className="locationInfo">
          <CardText>
            <CardTitle><h1>Location</h1></CardTitle>
            <p>
              Name: <b>{LocationData.name}</b>
            </p>
            <p>
              Type: <b>{LocationData.type}</b>
            </p>
            <p>
              Dimension: <b>{LocationData.dimension}</b>
            </p>
            <br />
            <h1>Residents</h1>
            <div dangerouslySetInnerHTML={{ __html: LocationResidents }} />
          </CardText>
        </CardBody>
      </Card>
    </div>
  );
}

export default LocationInfo;
