import React, { useState, useEffect } from "react";
import {
  Card, CardImg, CardTitle, CardGroup,
  CardBody
} from 'reactstrap';
import axios from "axios";
import "./Home.css";

const Home = () => {
  const [rick, setRick] = useState([]);
  const [morty, setMorty] = useState([]);
  const [summer, setSummer] = useState([]);
  const [beth, setBeth] = useState([]);
  const [jerry, setJerry] = useState([]);

  const [rickImage, setRickImage] = useState([]);
  const [mortyImage, setMortyImage] = useState([]);
  const [summerImage, setSummerImage] = useState([]);
  const [bethImage, setBethImage] = useState([]);
  const [jerryImage, setJerryImage] = useState([]);

  const FetchData = () => {
    const charRick = "https://rickandmortyapi.com/api/character/1";
    const charMorty = "https://rickandmortyapi.com/api/character/2";
    const charSummer = "https://rickandmortyapi.com/api/character/3";
    const charBeth = "https://rickandmortyapi.com/api/character/4";
    const charJerry = "https://rickandmortyapi.com/api/character/5";

    const charRickImage = "https://rickandmortyapi.com/api/character/avatar/1.jpeg";
    const charMortyImage = "https://rickandmortyapi.com/api/character/avatar/2.jpeg";
    const charSummerImage = "https://rickandmortyapi.com/api/character/avatar/3.jpeg";
    const charBethImage = "https://rickandmortyapi.com/api/character/avatar/4.jpeg";
    const charJerryImage = "https://rickandmortyapi.com/api/character/avatar/5.jpeg";
    
    const getRick = axios.get(charRick)
    const getMorty = axios.get(charMorty)
    const getSummer = axios.get(charSummer)
    const getBeth = axios.get(charBeth)
    const getJerry = axios.get(charJerry)

    const getRickImage = axios.get(charRickImage)
    const getMortyImage = axios.get(charMortyImage)
    const getSummerImage = axios.get(charSummerImage)
    const getBethImage = axios.get(charBethImage)
    const getJerryImage = axios.get(charJerryImage)


    axios.all([getRick, getMorty, getSummer, getBeth, getJerry, getRickImage, getMortyImage, getSummerImage, getBethImage, getJerryImage]).then(
      axios.spread((...allData) => {
        const charDataRick = allData[0].data.name
        const charDataMorty = allData[1].data.name
        const charDataSummer = allData[2].data.name
        const charDataBeth = allData[3].data.name
        const charDataJerry = allData[4].data.name

        const charImageRick = allData[0].data.image
        const charImageMorty = allData[1].data.image
        const charImageSummer = allData[2].data.image
        const charImageBeth = allData[3].data.image
        const charImageJerry = allData[4].data.image

        setRick(charDataRick)
        setMorty(charDataMorty)
        setSummer(charDataSummer)
        setBeth(charDataBeth)
        setJerry(charDataJerry)

        setRickImage(charImageRick)
        setMortyImage(charImageMorty)
        setSummerImage(charImageSummer)
        setBethImage(charImageBeth)
        setJerryImage(charImageJerry)
      })
    )
  }
  useEffect(() => {
    FetchData()
  }, [])

  return (
    <div className="header">
      <h1>Main Characters</h1>

    <CardGroup>
      <a className="link mainCharacterCards" href={"characters/1"}>
        <Card>
          <CardImg top width="100%" src={rickImage} alt="Rick image" />
          <CardBody className="CardBodyColor">
            <CardTitle tag="h3">{rick}</CardTitle>
          </CardBody>
        </Card>
      </a>

      <a className="link mainCharacterCards" href={"characters/2"}>
        <Card>
          <CardImg top width="100%" src={mortyImage} alt="Morty image" />
          <CardBody className="CardBodyColor">
            <CardTitle tag="h3">{morty}</CardTitle>
          </CardBody>
        </Card>
      </a>

      <a className="link mainCharacterCards" href={"characters/3"}>
        <Card>
          <CardImg top width="100%" src={summerImage} alt="Summer image" />
          <CardBody className="CardBodyColor">
            <CardTitle tag="h3">{summer}</CardTitle>
          </CardBody>
        </Card>
      </a>
      
      <a className="link mainCharacterCards" href={"characters/4"}>
        <Card>
          <CardImg top width="100%" src={bethImage} alt="Beth image" />
          <CardBody className="CardBodyColor">
            <CardTitle tag="h3">{beth}</CardTitle>
          </CardBody>
        </Card>
      </a>

      <a className="link mainCharacterCards" href={"characters/5"}>
        <Card>
          <CardImg top width="100%" src={jerryImage} alt="Jerry image" />
          <CardBody className="CardBodyColor">
            <CardTitle tag="h3">{jerry}</CardTitle>
          </CardBody>
        </Card>
      </a>

    </CardGroup>
  </div>
 )
}

export default Home;
