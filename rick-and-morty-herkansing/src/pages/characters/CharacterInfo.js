import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Card, CardImg, CardTitle, CardText, CardBody } from "reactstrap";
import "./Characters.css";

function CharacterInfo() {
  const { id } = useParams();
  const url = "https://rickandmortyapi.com/api/character/" + id;
  const [charData, setCharData] = useState([]);
  const [charOrigin, setCharOrigin] = useState([]);
  const [charLocation, setCharLocation] = useState([]);
  const [charEpisodes, setCharEpisodes] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const request = await axios.get(url);
      setCharData(request.data);
      setCharOrigin(request.data.origin);
      setCharLocation(request.data.location);
      setCharEpisodes(request.data.episode);

      let episodeUrls = [];
      let episodeText = "";
      for (var i = 0; i < request.data.episode.length; i++) {
        episodeUrls.push(axios.get(request.data.episode[i]));
      }

      axios.all(episodeUrls).then((response) => {
        response.forEach(function (res) {
          episodeText = episodeText += `<a href="/episodes/${res.data.id}"> 
                                          <div class="resident">
                                              ${res.data.name}
                                          </div>
                                        </a>`;
        });
        setCharEpisodes(episodeText);
      });

      return request;
    }
    fetchData();
  }, [url]);

  console.log(charData);

  return (
    <div className="container">
      <Card className="characterCard">
        <CardBody className="characterCardInfo">
          <div className="floatRight">
            <CardImg
              src={charData.image}
              className="imageSize img-fluid rounded-start"
              alt={charData.name}
            />
          </div>
          <CardText className="col-md-7 floatLeft">
            <CardTitle><h1>Character</h1></CardTitle>
            <p>
              Name: <b>{charData.name}</b>
            </p>
            <p>
              Species: <b>{charData.species}</b>
            </p>
            <p>
              Gender: <b>{charData.gender}</b>
            </p>
            <p>
              Origin: <b>{charOrigin.name}</b>
            </p>
            <p>
              Status: <b>{charData.status}</b>
            </p>
            <p>
              <a className="link" href={charLocation.url} >
              Location: <b>{charLocation.name}</b>
              </a>
            </p>
            <br />
            <div className="col-md-12">
              <h1>Episodes</h1>
              <div dangerouslySetInnerHTML={{ __html: charEpisodes }} />
            </div>
          </CardText>
        </CardBody>
      </Card>
    </div>
  );
}

export default CharacterInfo;
