import React, { useState, useEffect } from "react";
import axios from "axios";
import { Card, CardTitle, CardGroup, CardBody, Button } from "reactstrap";
import "./Episodes.css";


function Episodes() {
  const [url] = useState("https://rickandmortyapi.com/api/episode/");
  const [info, setInfo] = useState({});
  const [results, setResults] = useState([]);
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(1);

  useEffect(() => {
    console.log("url: ", url);
    console.log("info: ", info);
    console.log("results: ", results);
    console.log("search: ", search);
  }, [url, info, results, search]);

  useEffect(() => {
    axios
      .get(`${url}?page=${page}&name=${search}`)
      .then((result) => {
        // console.log(result)
        setInfo(result.data.info);
        setResults(result.data.results);
      })
      .catch((error) => {
        console.log(error);
        setPage(1);
      });
  }, [url, search, page]);

  const nextHandler = (event) => {
    event.preventDefault();
    if (page <= info.pages) {
      setPage(page + 1);
    } else {
      setPage(1);
    }
  };

  const prevHandler = (event) => {
    event.preventDefault();
    if (page > 1) {
      setPage(page - 1);
    } else {
      setPage(info.pages);
    }
  };

  return (
    <>
      <header className="pagination">
        <Button
          className="paginationButton"
          color="danger"
          onClick={(event) => prevHandler(event)}
        >
          -
        </Button>

        <div className="paginationInfo">
          <p>
            {page}/{info.pages}
          </p>
        </div>
        <Button
          className="paginationButton"
          color="success"
          onClick={(event) => nextHandler(event)}
        >
          +
        </Button>

        <div className="form-inline">
          <input
            className="form-control mr-sm-2"
            onChange={(e) => {
              setSearch(e.target.value);
            }}
            value={search}
            type="text"
            placeholder="Search"
          />
        </div>
      </header>
      <CardGroup className="container">
        {results.map((episode, index) => (
          <a className="link" href={"episodes/" + episode.id}>
            <Card className="cardSize">
              <CardBody className="CardBodyColor">
                <CardTitle tag="h3">{episode.name}</CardTitle>
              </CardBody>
            </Card>
          </a>
        ))}
      </CardGroup>
    </>
  );
}

export default Episodes;
