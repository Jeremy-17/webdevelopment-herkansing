import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Card, CardTitle, CardText, CardBody } from "reactstrap";
import "./Episodes.css";

function EpisodeInfo() {
  const { id } = useParams();
  const url = "https://rickandmortyapi.com/api/episode/" + id;
  const [EpisodeData, setEpisodeData] = useState([]);
  const [CharacterEpisode, setCharacterEpisode] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const request = await axios.get(url);
      setEpisodeData(request.data);

      let charactersUrls = [];
      let characterText = "";
      for (var i = 0; i < request.data.characters.length; i++) {
        charactersUrls.push(axios.get(request.data.characters[i]));
      }

      axios.all(charactersUrls).then((response) => {
        response.forEach(function (res) {
          characterText =
            characterText += `<a href="/characters/${res.data.id}"> 
                                <div class="character">
                                  <img src="${res.data.image}"/>
                                  ${res.data.name}
                                </div>
                              </a>`;                          
        });
        setCharacterEpisode(characterText);
      });

      return request;
    }
    fetchData();
  }, [url]);

  console.log(EpisodeData);

  return (
    <div className="container">
      <Card className="episode">
        <CardBody className="episodeInfo">
          <CardText>
            <CardTitle>
              <h1>Episode</h1>
            </CardTitle>

            <p>
              Name: <b>{EpisodeData.name}</b>
            </p>
            <p>
              Air Date: <b>{EpisodeData.air_date}</b>
            </p>
            <p>
              Episode: <b>{EpisodeData.episode}</b>
            </p>
            <br />
            <h1>Characters</h1>
            <div dangerouslySetInnerHTML={{ __html: CharacterEpisode }} />
          </CardText>
        </CardBody>
      </Card>
    </div>
  );
}

export default EpisodeInfo;
