import { BrowserRouter, Route } from "react-router-dom";
import "./App.css";
import Nav from "./components/Nav"
import Home from "./pages/home/Home";
import Characters from "./pages/characters/Characters"
import CharacterInfo from "./pages/characters/CharacterInfo"
import Locations from "./pages/locations/Locations"
import LocationInfo from "./pages/locations/LocationInfo"
import Episodes from "./pages/episodes/Episodes"
import EpisodesInfo from "./pages/episodes/EpisodeInfo"



function App() {
  return (
    <BrowserRouter>
      <div className="App">
       <Nav />
      </div>
      <Route path={"/"} component={Home} exact />
      <Route path={"/Characters"} component={Characters} exact />
      <Route path={"/characters/:id"} component={CharacterInfo} />
      <Route path={"/locations"} component={Locations} exact />
      <Route path={"/locations/:id"} component={LocationInfo} />
      <Route path={"/episodes"} component={Episodes} exact />
      <Route path={"/episodes/:id"} component={EpisodesInfo} />
    </BrowserRouter>
  );
}

export default App;
